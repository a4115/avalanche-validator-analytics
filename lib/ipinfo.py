import ipinfo
import pycountry
import ipaddress
import pandas as pd

from lib.constants import *


def valid_ip(ip_address):
    try:
        return ipaddress.ip_address(ip_address)
    except:
        return False


def strip_port(ip_address):
    if "." in ip_address:
        return ip_address.split(":")[0]
    else:  # we assumes is IPv6
        return ip_address.split("]")[0].split("[")[1]


def get_ipinfo(ip_address):
    handler = ipinfo.getHandler(ACCESS_TOKEN)
    if not valid_ip(ip_address):
        return

    location = handler.getDetails(ip_address)
    if (
        hasattr(location, "country")
        & hasattr(location, "country_name")
        & hasattr(location, "org")
    ):
        return {
            "ip": location.ip,
            "country_code": location.country,
            "country": location.country_name,
            "org": location.org,
        }


def iso2_iso3(countries_df):
    # group by country, change the code country from iso2 to iso 3

    iso3_list = []
    for code in countries_df["country_code"]:
        iso3 = pycountry.countries.get(alpha_2=code).alpha_3
        iso3_list.append(iso3)
    countries_df["iso3"] = iso3_list
    return countries_df


def ip_details(ip_list_without_ports):

    # obtain ip details
    df = pd.DataFrame()
    for ip in ip_list_without_ports:
        # print(ip)
        geolocation = get_ipinfo(ip)
        print(geolocation)
        if geolocation:
            df = pd.concat(
                [df, pd.DataFrame(geolocation, index=[0])], ignore_index=True
            )

    df.to_json(NODES_DETAIL_FILE)  # save data in a file
    return df


def ip_details_country(df):

    countries_df = (
        df.groupby(["country_code", "country"]).size().reset_index(name="count")
    )

    countries_df = iso2_iso3(countries_df)

    validators = countries_df["count"].sum()
    print(validators)

    countries_df.to_json(NODES_GEOJSON_FILE)  # save data in a file

    return countries_df
