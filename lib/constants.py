# ____CALL AVAX API____
#PORT = "443"
#IP = "api.avax.network"
BASE_URL= f"https://api.avax.network"
# INFO_URL= f"{BASE_URL}/ext/info"
# P_URL = f"{BASE_URL}/ext/bc/P"

# ____FILE DETAILS____
NODES_DETAIL_FILE = "./output/node_details.json"
NODES_GEOJSON_FILE = "./output/geo_results.json"
WORLD_GEOJSON_FILE = "./input/countries.geojson.json"
IMAGES_PATH = "./output/images"

# ____ZOOM DETAILS____
ZOOM_STAKEAMOUNT = 20000
ZOOM_FEE = 20

# ___TOKEN IPINFO API____
ACCESS_TOKEN = ""
