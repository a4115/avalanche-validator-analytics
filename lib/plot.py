import plotly.express as px
import json
import os

from lib.constants import *


def stake_vs_delegation(data_plot, args):
    # Plot StakeAmount (hist)
    fig = px.histogram(x=data_plot["stakeAmount"], title="Validators stake amount")
    maybe_save_show(args, fig, "stake_histogram")

    # Histogram delegationRate
    fig = px.histogram(
        x=data_plot["delegationRate"],
        title="Delegation Rate",
        labels={"x": "Delegation Rate", "y": "count"},
    )

    maybe_save_show(args, fig, "delegation_histogram")

    # Plot StakeAmount vs delegationRate
    fig = px.scatter(
        data_plot,
        x="stakeAmount",
        y="delegationRate",
        title="Stake amount vs delegation rate",
    )

    maybe_save_show(args, fig, "stake_delegation")


def stake_vs_delegation_zoom(data_plot, args):

    # Plot StakeAmount< Min AVAX (hist)
    stake_zoom = data_plot[data_plot["stakeAmount"] < ZOOM_STAKEAMOUNT]
    fig = px.histogram(
        x=stake_zoom["stakeAmount"],
        title="Validators stake amount (<20,000 AVAX)",
        labels={"x": "Stake amount", "y": "count"},
    )

    maybe_save_show(args, fig, "stake_histogram(zoom)")

    # Plot delegationRate< Min AVAX (hist)
    fig = px.histogram(
        x=stake_zoom["delegationRate"],
        title="DelegationRate (zoom stakeAmount<20,000 AVAX)",
        labels={"x": "Delegation Rate", "y": "count"},
    )

    maybe_save_show(args, fig, "histogram_delegation(zoom)")

    # Plot StakeAmount vs delegationRate (above min AVAX)
    fig = px.scatter(
        stake_zoom,
        x="stakeAmount",
        y="delegationRate",
        title="Stake amount vs delegation rate (zoom <20,000 AVAX)",
    )

    maybe_save_show(args, fig, "stake_delegation(zoom)")


def fees_vs_delegation(data_plot, args):

    # Plot Fees (<20) vs delegationRate
    fee_zoom = data_plot[data_plot["delegationFee"] < ZOOM_FEE]

    # sns.scatterplot(data=data_plot, x="delegationFee", y="delegationRate")
    fig = px.scatter(
        fee_zoom,
        x="delegationFee",
        y="delegationRate",
        title="Delegation fee vs Delegation rate",
    )

    maybe_save_show(args, fig, "fees_delegation")

    # Plot Fees (<20) vs delegationRate (mean)
    fee_zoom["n_validators"] = None
    fee_mean = (
        fee_zoom.groupby("delegationFee")
        .agg({"delegationRate": "mean", "stakeAmount": "mean", "n_validators": "size"})
        .rename(
            columns={
                "delegationRate": "delegationRate_mean",
                "stakeAmount": "stakeAmount_mean",
            }
        )
        .reset_index()
    )

    fee_mean = fee_mean[fee_mean["n_validators"] > 5]  # data with min 10 validators
    fig = px.scatter(
        fee_mean,
        x="delegationFee",
        y="delegationRate_mean",
        size="n_validators",
        title="Delegation Fee vs Delegation rate(mean)",
    )

    maybe_save_show(args, fig, "fees_delegation(mean)")

    # Plot Fee vs stake amount vs delegation rate
    fig = px.scatter(
        fee_zoom,
        x="stakeAmount",
        y="delegationFee",
        color="delegationRate",
        title="Fee, stake amount and delegation rate analysis",
    )

    maybe_save_show(args, fig, "fees_stake_delegation")

    fee_stake_zoom = fee_zoom[fee_zoom["stakeAmount"] < ZOOM_STAKEAMOUNT]
    fig = px.scatter(
        fee_stake_zoom,
        x="stakeAmount",
        y="delegationFee",
        color="delegationRate",
        title="Fee, stake amount and delegation rate analysis (zoom 20,000 AVAX)",
    )

    maybe_save_show(args, fig, "fees_stake_delegation(zoom)")

    # Plot Fee vs stake amount (mean) vs delegation rate (mean)

    fig = px.scatter(
        fee_mean,
        x="stakeAmount_mean",
        y="delegationFee",
        color="delegationRate_mean",
        size="n_validators",
        title="Fee, stake amount (mean) and delegation rate (mean) analysis (zoom 20,000 AVAX)",
    )

    maybe_save_show(args, fig, "fees_stake_delegation(mean)")


def plot_geolocation(countries_df, column, args, name):
    with open(WORLD_GEOJSON_FILE) as f:
        countries = json.load(f)

    fig = px.choropleth(
        countries_df.sort_values(by=[column], ascending=False),
        color_continuous_scale="reds",
        geojson=countries,
        locations="iso3",
        featureidkey="properties.ISO_A3",
        color=column,
        scope="world",
        projection="natural earth",
        range_color=(0, max(countries_df[column])),
        hover_data=[column],
    )

    maybe_save_show(args, fig, name)


def maybe_save_show(args, fig, name):
    if not args.no_show:
        fig.show()
    if not args.save:
        return
    if not os.path.exists(IMAGES_PATH):
        os.mkdir(IMAGES_PATH)
    fig.write_image(f"{IMAGES_PATH}/{name}.png")
