import pandas as pd
import requests
import plotly.express as px
import argparse

from urllib.request import urlopen
from lib.constants import *
from lib.plot import *
from lib.ipinfo import *
from lib.secrets import *


def call_avax_api(url, method, params):
    payload = {"jsonrpc": "2.0", "method": method, "params": params, "id": "1"}
    headers = {"Content-Type": "application/json"}
    
    response = requests.post(url, json=payload, headers=headers).json()
    return response


def convert_nAVAX(df):
    # 1 nAVAX is equal to 0.000000001 AVAX
    df_resultado = df * 0.000000001
    return df_resultado


def delegation_rate(vdf):
    # The maximum weight of a validator (their own stake + stake delegated to them)
    # is the minimum of 3 million AVAX  and 5 times the amount the validator staked.

    # selecting columns df[['col1', 'col2']] and pre-treat the data
    vdf_weight = vdf[["stakeAmount", "delegatorWeight"]]
    vdf_data = convert_nAVAX(vdf_weight)
    vdf_data = vdf_data[vdf_data["stakeAmount"] > 2000]
    n_validators = len(vdf_data)
    print(f"The number of current active validators are {n_validators}")

    # Calculate the delegationRate
    vdf_data["maximumDelegation"] = 4 * vdf_data["stakeAmount"]
    vdf_data["delegationRate"] = (
        vdf_data["delegatorWeight"] / vdf_data["maximumDelegation"]
    )
    vdf_data["delegationRate"] = vdf_data["delegationRate"] * 100
    data_plot = vdf_data[["stakeAmount", "delegationRate"]]
    data_plot["delegationFee"] = vdf["delegationFee"].astype(float)
    return data_plot


def fetch_current_validators(args):

    # Call API: Platform.getCurrentValidators

    P_URL = f"{args.base_url}/ext/bc/P"
    
    data = call_avax_api(P_URL, "platform.getCurrentValidators", None)

    vdf = pd.DataFrame(data["result"]["validators"])
    vdf[["stakeAmount", "delegatorWeight"]] = vdf[
        ["stakeAmount", "delegatorWeight"]
    ].astype(int)

    return vdf


def fetch_ip_data(args, vdf):
    
    INFO_URL = f"{args.base_url}/ext/info"

    data_IP = call_avax_api(INFO_URL, "info.peers", None)  # Call API: info.peers
    df = pd.DataFrame(data_IP["result"]["peers"])
    ipdf = df[["publicIP", "nodeID"]]

    # Filter by current validators
    current_validators = ipdf["nodeID"].isin(vdf["nodeID"])

    ipdf_active = ipdf[current_validators].dropna()
    ip_list_without_ports = [strip_port(ip) for ip in ipdf_active["publicIP"]]

    if args.file == True:
        df = pd.read_json(NODES_DETAIL_FILE)
        countries_df = pd.read_json(NODES_GEOJSON_FILE)
    else:
        df = ip_details(ip_list_without_ports)
        countries_df = ip_details_country(df)

    # obtain a df with stake and country
    vdf_totalstake = vdf[["stakeAmount", "nodeID", "delegatorWeight"]]

    vdf_totalstake["stakeAmount"] = convert_nAVAX(vdf_totalstake["stakeAmount"])
    vdf_totalstake["delegatorWeight"] = convert_nAVAX(vdf_totalstake["delegatorWeight"])

    total_stake = vdf_totalstake["stakeAmount"].sum()
    total_delegation = vdf_totalstake["delegatorWeight"].sum()

    vdf_totalstake["totalStake"] = (
        vdf_totalstake["stakeAmount"] + vdf_totalstake["delegatorWeight"]
    )

    ipdf_active = ipdf_active.rename(columns={"publicIP": "ip"})
    ipdf_active["ip"] = ip_list_without_ports

    df_nodeid = pd.merge(df, ipdf_active, on="ip", how="left")

    df_stake = pd.merge(df_nodeid, vdf_totalstake, on="nodeID", how="left")
    print(df_stake)

    countries_df_stake = (
        df_stake.groupby(["country_code", "country"])
        .agg({"delegatorWeight": "sum", "totalStake": "sum", "stakeAmount": "sum"})
        .reset_index()
    )
    countries_df_stake = iso2_iso3(countries_df_stake)

    total_stake = countries_df_stake["stakeAmount"].sum()
    total_delegation = countries_df_stake["delegatorWeight"].sum()
    print(
        f"The total validators own stake is {total_stake} and the total delegation stake is {total_delegation}"
    )

    return df, countries_df, countries_df_stake


def stake_and_delegations_analysis(args, vdf):
    if args.model_id != "delegation" and args.model_id != "all":
        return

    # Process data
    data_plot = delegation_rate(vdf)

    # plot
    stake_vs_delegation(data_plot, args)
    stake_vs_delegation_zoom(data_plot, args)
    fees_vs_delegation(data_plot, args)

    return


def geolocalization_analysis(args, countries_df, countries_df_stake):
    if args.model_id != "geolocalization" and args.model_id != "all":
        return

    plot_geolocation(countries_df, "count", args, "validators_geolocalization")  # validators geolocalization
    plot_geolocation(countries_df_stake, "totalStake", args, "totalweight_geolocalization")
    plot_geolocation(countries_df_stake, "delegatorWeight", args, "delegation_geolocalization")
    plot_geolocation(countries_df_stake, "stakeAmount", args, "ownstake_geolocalization")


def asn_analysis(args, df):
    if args.model_id != "asn" and args.model_id != "all":
        return
    fig = px.histogram(x=df["org"], title="ASN Distribution")

    maybe_save_show(args, fig, "asn_distribution")


def ip_analysis(args, vdf):
    if args.model_id == "delegation":
        return

    df, countries_df, countries_df_stake = fetch_ip_data(args, vdf)
    geolocalization_analysis(args, countries_df, countries_df_stake)
    asn_analysis(args, df)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-u", "--base-url", default=BASE_URL, help="Avalanche Node URL")
    parser.add_argument(
        "-f",
        "--file",
        action="store_true",
        help="Use the default file containing the node details",
    )
    parser.add_argument(
        "-m",
        "--model-id",
        default="all",
        help="Avax analysis model to run: [delegation, geolocalization, asn, all]",
    )
    parser.add_argument(
        "-n", "--no-show", action="store_true", help="Don’t show plots in browser"
    )
    parser.add_argument(
        "-s", "--save", action="store_true", help="Save plots in images/ folder"
    )
    args = parser.parse_args()

    if args.no_show and not args.save:
        print("No show and no save? Are you sure?")
        exit(1)

    return args


def main():
    args = parse_args()

    vdf = fetch_current_validators(args)

    stake_and_delegations_analysis(args, vdf)

    ip_analysis(args, vdf)

    return


if __name__ == "__main__":
    main()
