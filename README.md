# Avalanche validator analysis

Avalanche staking involves locking AVAX tokens to support transaction processing and network security. Participants include validators and delegators. Validators run nodes to validate transactions and secure the network, while delegators can participate in staking by entrusting their tokens to validators.

This Python script offers insights into the current Avalanche staking framework. It generates charts analyzing stake distribution, delegation trends, validator locations, and hosting preferences.

The script provides valuable information for new validators and delegators. It helps them choose less represented geographical zones or hosting providers to enhance network decentralization. Additionally, it offers data on delegator stakes to help validators optimize their delegation rates.

The project comprises three main parts:

1. Data Fetching: Utilizing two calls to the Avalanche API to retrieve data.
2. Data Processing: Extracting stake amounts and delegation rates, ASN (Autonomous System Number), and validator geolocation.
3. Data Visualization: Plotting various charts (histograms, scatter plots, bubble charts, bar charts, and maps) using the Plotly library to illustrate correlations between different staking parameters (delegation rate, stake amount, fees, ASN or IP localization).

## Pre-requisites

You can install the dependencies using a set of versions that is known to work:

```
pip3 install -r requirements.txt
```

### Using a virtual environment

To create a new virtual environment in directory `.venv`:

```
python3 -m venv .venv
```

To use the virtual environment in your current shell:

```
source .venv/bin/activate
```

Once the environment is active, the `python3` and `pip3` commands can be used as usual but they will act inside the virtual environment. This means you can install dependencies in the same way as you would globally.

> **Note:** remember to activate your virtual environment everytime you want to work on the project in a new shell.

## Running the projects

In this project, we'll use the IPinfo API and the Python requests library to lookup the country for an IP address.Before to run, you'll need an IPinfo API access token, which you can get by signing up for a free account at https://ipinfo.io/signup. Then, you will define the ACCESS_TOKEN variable.


```
python3 ./validators_analysis.py
```

## Command line options

The CLI options of the script can be displayed by running --help. All options are also covered in more detail below.

-u ,--base-url

By default, the script uses the public Avalanche API server. There is a public API server that allows developers to access the Avalanche network without having to run a node themselves. The public API server is at https://api.avax.network for Avalanche Mainnet

-f | --file
The data retrieved from the ipinfo API call is stored in `NODES_DETAIL_FILE`. You have the option to utilize this file from the previous call, thus avoiding the need to make another API call.

-m | --model-id

Specify the validator`s analysis to be conducted

- "delegation"
- "geolocalization"
- "asn"
- "all"

-n | --no-show
Don´t show plots in browser. Note that you should use this option with -s.

-s | --save

Save plots in `IMAGES_PATH`(variable define in `constants.py`). By default this variable is set to `./output/images`